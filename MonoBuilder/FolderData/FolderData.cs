using System.IO;

namespace MonoBuilder
{
    public class FolderData
    {
        private DirectoryInfo _info;

        /// <summary>Gets the DirectoryInfo of the directory.</summary>
        /// <returns>A DirectoryInfo of the current FolderData.</returns>
        public DirectoryInfo Info => _info;
        
        /// <summary>Gets the full path of the directory.</summary>
        /// <returns>A string containing the full path.</returns>
        public string Path => _info.FullName;
        
        /// <summary>Returns a file list from the current directory.</summary>
        /// <returns>An array of type <see cref="T:System.IO.FileInfo" />.</returns>
        public virtual FileInfo[] Files => _info.GetFiles();

        /// <summary>
        /// Initializes a new instance of the class on the specified path.
        /// </summary>
        /// <param name="path">A string specifying the path on which to create the FolderData</param>
        public FolderData(string path)
        {
            _info = Directory.Exists(path) ? new DirectoryInfo(path) : Directory.CreateDirectory(path);
        }

        public string CombinePath(string path)
        {
            return System.IO.Path.Combine(Path, path);
        }
    }
}
using System.IO;

namespace MonoBuilder
{
    public class ScriptsFolderData : FolderData
    {
        public override FileInfo[] Files => Info.GetFiles("*.cs");

        public ScriptsFolderData(string path) : base(path)
        {
        }
    }
}
using System.IO;

namespace MonoBuilder
{
    public class AssembliesFolderData : FolderData
    {
        public override FileInfo[] Files => Info.GetFiles("*.dll");

        public AssembliesFolderData(string path) : base(path)
        {
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MonoLogic
{
    public class HelloWorld
    {
        public string Message = "Testix";

        private static List<Func<dynamic>> _tests = new List<Func<dynamic>>()
        {
            () => Varwin.VString.Concat("test", "empire"),
            () => Varwin.VString.Length("respect"),
            () => Varwin.VString.IndexOf("All rights reserved", "rights"),
            () => Varwin.VString.IndexOf("All rights 2105 reserved", 2105),
            () => Varwin.VMath.Abs(-42),
            () => Varwin.VMath.RandomDouble(),
            () => Varwin.VMath.RandomInt(-100, 100),
        };

        public HelloWorld()
        {
            Log("HelloWorld instance created!");
            
            for (int i = 0; i < _tests.Count; i++)
            {
                Log("    " + i + ") " + _tests[i].Invoke());
            }
        }

        public void Hello()
        {
            Log(Varwin.VString.Concat("test", "empire"));
            Log(Varwin.VString.Length("respect"));
            Log(Varwin.VString.IndexOf("All rights reserved", "rights"));
            Log(Varwin.VString.IndexOf("All rights 2105 reserved", 2105));
            Log(Message + ": invoked in HelloWorld.Hello()");
        }

        private void Log(object message)
        {
            string prefix = $"[{Time.time.ToString("0.0")}]".PadRight(8);
            Debug.Log(prefix + message);
            Console.WriteLine(prefix + message.ToString());
        }
    }
}
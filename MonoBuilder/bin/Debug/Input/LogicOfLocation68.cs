using System;
using System.Collections.Generic;
using UnityEngine;

namespace Varwin
{
    public class LogicOfLocation68 : ILogic
    {
        private WrappersCollection collection;

        private dynamic _testInteger1;
        private dynamic _testInteger2;
        private dynamic _randomInteger;

        private dynamic _randomDouble;

        private dynamic _testString1;
        private dynamic _testString2;

        private dynamic _display;

        private float _timer;

        public void SetCollection(WrappersCollection collection)
        {
            this.collection = collection;
        }

        public void Initialize()
        {
            Log("Initialize of LogicOfLocation68");

            _testInteger1 = UnityEngine.Random.Range(100, 1000);
            _testInteger2 = UnityEngine.Random.Range(100, 1000);

            _testString1 = "app id " + UnityEngine.Application.identifier;
            _testString2 = "company " + UnityEngine.Application.companyName;

            GameObject go = GameObject.Find("TestDisplay");
            if (go != null)
            {
                _display = go.GetComponent("SimpleDisplay");
            }

            SetMessage();
        }

        public void Update()
        {
            _timer += UnityEngine.Time.deltaTime;

            if (_timer > 2f)
            {
                _timer = _timer % 2f;
                SetMessage();
            }
        }

        public void Events()
        {
            
        }

        private void SetMessage()
        {
            if (_display != null)
            {
                _display.DisplayText = GetMessage();
            }
        }

        private void Log(object message)
        {
            string prefix = $"[{Time.time.ToString("0.0")}]".PadRight(8);
            Debug.Log(prefix + message);
            Console.WriteLine(prefix + message.ToString());
        }

        private string GetMessage()
        {
            _randomInteger = VMath.RandomInt(_testInteger1, _testInteger2);
            
            _randomDouble = VMath.RandomDouble();

            string s = "";
            s += GetMessageRow("Time.time", UnityEngine.Time.time);
            s += GetMessageRow("_timer", _timer);
            s += "\n";
            s += GetMessageRow("_testInteger1", _testInteger1);
            s += GetMessageRow("_testInteger2", _testInteger2);
            s += GetMessageRow("_randomInteger", _randomInteger);
            s += "\n";
            s += GetMessageRow("_randomDouble", _randomDouble);
            s += "\n";
            s += GetMessageRow("_testString1", _testString1);
            s += GetMessageRow("_testString2", _testString2);
            s += GetMessageRow("concat.ToTitleCase", VString.ToTitleCase(VString.Concat(_testString1, _testString2)));
            return s;
        }

        private string GetMessageRow(object name, object value)
        {
            return $"<b>{name}</b>\n\t= {value}\n";
        }
    }
}

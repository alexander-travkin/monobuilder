﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Varwin.Experemental;

namespace MonoBuilder
{
    internal class Program
    {
        /// <summary>
        /// Backend-utility for compile cs-scripts to dll-libraries
        /// </summary>
        /// <param name="args">File names from the Input folder for build. If args is empty, run builds for all scripts from the Input folder</param>
        public static void Main(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                var allInputFiles = Resources.Input.Files;
                var targetFiles = new List<FileInfo>();
                    
                for (int i = 0; i < args.Length; ++i)
                {
                    string filename = args[i];
                    var fileInfo = allInputFiles.FirstOrDefault(x => string.Equals(x.Name, filename, StringComparison.Ordinal));

                    if (fileInfo == null)
                    {
                        Console.WriteLine($"Target {filename} can't be added!");
                    }
                    else
                    {
                        Console.WriteLine($"Add target {filename} ({fileInfo.Name})");
                        targetFiles.Add(fileInfo);
                    }
                }
                Console.WriteLine();

                if (targetFiles.Count > 0)
                {
                    Compile(targetFiles.ToArray());
                }
                else
                {
                    Console.WriteLine($"Nothing to build!");
                }
            }
            else
            {
                Compile(Resources.Input.Files);
            }
        }

        /// <summary>
        /// Start compiling the specified files
        /// </summary>
        /// <param name="scripts">Files to compile</param>
        public static void Compile(FileInfo[] scripts)
        {
            foreach (FileInfo script in scripts)
            {
                string filepath = script.FullName.Replace(Resources.Root.Path, "").Trim('\\');
                Console.WriteLine($"Compile {script.Name} (\"{filepath}\")");
                
                var now = DateTime.Now;
                
                var errors = new List<CompilerError>();
                var assembly = RuntimeCompiler.CompileFile(script, ref errors);

                if (assembly != null)
                {
                    Console.WriteLine($"Compilation of {script.Name} is finished after {DateTime.Now - now}");
                }
                else
                {
                    Console.WriteLine($"Compilation of {script.Name} is failed!");
                    foreach (var error in errors)
                    {
                        Console.WriteLine($" - {error}");
                    }
                }
                Console.WriteLine();
            }
        }
        
        /// <summary>
        /// Test method. Invoke specified method from instance of the first class from specified assembly
        /// </summary>
        /// <param name="file">Assembly-name from the Output folder</param>
        /// <param name="method">Invoked method name</param>
        public static void Test(string file, string method)
        {
            Assembly assembly = Assembly.LoadFile(Resources.Output + "\\" + file);
            Type t = assembly.ExportedTypes.FirstOrDefault();
            var instance = Activator.CreateInstance(t);
            var m = t.GetMethod(method);
            m.Invoke(instance, null);
        }
    }
}
using System.IO;
using System.Net;

namespace MonoBuilder.Extensions
{
    public static class FileInfoExtensions
    {
        public static string GetNameWithoutExtension(this FileInfo file)
        {
            int length;
            if ((length = file.Name.LastIndexOf('.')) == -1)
                return file.Name;
            return file.Name.Substring(0, length);
        }

        /// <summary>
        /// Moves a specified file to a new location, providing the option to specify a new file name.
        /// </summary>
        /// <param name="fileInfo">Moving file</param>
        /// <param name="directoryInfo">Target folder</param>
        /// <param name="overrideFile">Override file if exists</param>
        public static void MoveTo(this FileInfo fileInfo, DirectoryInfo directoryInfo, bool overrideFile = false)
        {
            string path = Path.Combine(directoryInfo.FullName, fileInfo.Name);
            
            var dir = Path.GetDirectoryName(path);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            
            if (overrideFile)
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            
            fileInfo.MoveTo(path);
        }
    }
}
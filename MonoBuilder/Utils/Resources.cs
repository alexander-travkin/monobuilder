using System.IO;
using System.Reflection;

namespace MonoBuilder
{
    public static class Resources
    {
        private const string InputFolderName = "Input";
        private const string OutputFolderName = "Output";
        private const string ReferencesFolderName = "References";
        
        /// <summary>
        /// The program's exe file
        /// </summary>
        public static readonly FileInfo Exe = new FileInfo(Assembly.GetExecutingAssembly().Location);
        
        /// <summary>
        /// The Root folder of the program
        /// </summary>
        public static readonly FolderData Root = new FolderData(Exe.Directory.FullName);

        /// <summary>
        /// The Input folder with CSharp-scripts
        /// </summary>
        public static readonly ScriptsFolderData Input = new ScriptsFolderData(Root.CombinePath(InputFolderName));
        
        /// <summary>
        /// The Output folder with compiled assemblies (dll)
        /// </summary>
        public static readonly AssembliesFolderData Output = new AssembliesFolderData(Root.CombinePath(OutputFolderName));
        
        /// <summary>
        /// The References folder with assemblies required for compiling scripts
        /// </summary>
        public static readonly AssembliesFolderData References = new AssembliesFolderData(Root.CombinePath(ReferencesFolderName));
    }
}
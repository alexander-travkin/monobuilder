using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Modified.Mono.CSharp;
using MonoBuilder;
using MonoBuilder.Extensions;

namespace Varwin.Experemental
{
    public static class RuntimeCompiler
    {
        private static readonly Dictionary<string, string> Assemblies = new Dictionary<string, string>();

        static RuntimeCompiler()
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    string dll = Path.GetFileName(assembly.Location);

                    if (!Assemblies.ContainsKey(dll))
                    {
                        Assemblies.Add(dll, assembly.Location);
                    }
                }
                catch (Exception e)
                {
                    //
                }
            }
            
            foreach (var reference in Resources.References.Files)
            {
                try
                {
                    if (!Assemblies.ContainsKey(reference.Name))
                    {
                        Assemblies.Add(reference.Name, reference.FullName);
                    }
                }
                catch (Exception e)
                {
                    //
                }
            }
        }
        
        public static Assembly CompileFile(FileInfo file, ref List<CompilerError> errors)
        {
            string code = File.ReadAllText(file.FullName);
            return Compile(code, file.GetNameWithoutExtension(), ref errors);
        }
        
        public static Assembly Compile(string code, string outputAssemblyName, ref List<CompilerError> errors)
        {
            var cSharpCodeCompiler = new CSharpCodeCompiler();
            CompilerParameters parameters = GetCompilerParameters(outputAssemblyName);
            CompilerResults result = cSharpCodeCompiler.CompileAssemblyFromSource(parameters, code);

            if (!result.Errors.HasErrors)
            {
                var compiledAssembly = new FileInfo(outputAssemblyName + ".dll");
                
                compiledAssembly.MoveTo(Resources.Output.Info, true);
                
                return result.CompiledAssembly;
            }

            foreach (CompilerError error in result.Errors)
            {
                errors.Add(error); 
            }

            return null;
        }

        private static CompilerParameters GetCompilerParameters(string outputAssemblyName)
        {
            var parameters = new CompilerParameters
            {
                GenerateInMemory = true,
                GenerateExecutable = false,
                IncludeDebugInformation = false,
                OutputAssembly = outputAssemblyName + ".dll",
            };

            foreach (var assembly in Assemblies)
            {
                try
                {
                    if (!parameters.ReferencedAssemblies.Contains(assembly.Value))
                    {
                        parameters.ReferencedAssemblies.Add(assembly.Value);
                    }
                }
                catch (Exception e)
                {
                    //
                }
            }

            return parameters;
        }
    }
}
